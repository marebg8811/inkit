import React, { FunctionComponent, useCallback, useState } from 'react';
import { CommentBoxType } from './types';
import { uuid } from 'uuidv4';

const CommentBox: FunctionComponent<CommentBoxType> = ({
    addComment,
    parentId,
}) => {
    const [comment, setComment] = useState({
        body: '',
        title: '',
    });

    const [isFormVisible, setFormVisibility] = useState(false);

    const submit = (e: any) => {
        e.preventDefault();
        addComment(comment, parentId, uuid());
        clearComment();
        setFormVisibility(false);
    };

    const isDisabled = !comment.body || !comment.title;

    const clearComment = useCallback(
        () => setComment({ body: '', title: '' }),
        []
    );

    return (
        <div className='flex flex-col py-4 text-base text-black'>
            {!isFormVisible && (
                <button
                    className='bg-green-600 ml-2 py-2 px-1 max-w-xs'
                    onClick={() => setFormVisibility(true)}>
                    Add Comment
                </button>
            )}

            {isFormVisible && (
                <div className='flex flex-col'>
                    <input
                        className='mb-4 px-2 py-1 outline-none'
                        placeholder='Title'
                        type='text'
                        value={comment.title}
                        onChange={e =>
                            setComment({ ...comment, title: e.target.value })
                        }
                    />
                    <textarea
                        className='p-3 outline-none'
                        spellCheck='false'
                        placeholder='Add you comment here...'
                        value={comment.body}
                        onChange={e =>
                            setComment({ ...comment, body: e.target.value })
                        }
                    />

                    <div className='buttons flex mt-10'>
                        <button
                            className='border p-1 px-4 text-gray-500 ml-auto '
                            onClick={clearComment}>
                            Cancel
                        </button>
                        <button
                            className='bg-green-600 ml-2 px-4 '
                            disabled={isDisabled}
                            onClick={submit}>
                            Save Comment
                        </button>
                    </div>
                </div>
            )}
        </div>
    );
};

export default CommentBox;
