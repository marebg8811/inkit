import { uuid } from 'uuidv4';
import { CommentType } from '../../types';

export type CommentBoxType = {
    className: string;
    addComment: (
        comment: CommentType,
        parentId: string | undefined,
        uuid: string
    ) => void;
    parentId?: string | undefined;
};
