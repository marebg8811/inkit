import React, { FunctionComponent, memo } from 'react';

import CommentBox from '../CommentBox';
import { CommentType } from '../../types';
import { CommentListTypes } from './types';

const CommentList: FunctionComponent<CommentListTypes> = ({
    comments,
    addComment,
}) => {
    const hasReplays = (replays: CommentType[]) => {
        return replays && replays.length;
    };

    return (
        <>
            {comments.map(({ uuid, title, body, replays }) => {
                return (
                    <div
                        key={uuid}
                        className='border-l mb-5 flex flex-col pl-5 max-w-2xl my-10 break-words w-10/12'>
                        <h5 className='self-start text-3xl'>{title}</h5>
                        <p className='text-left text-base'>{body}</p>

                        <CommentBox
                            className='mt-10 self-start text-green-900'
                            parentId={uuid}
                            addComment={addComment}
                        />

                        {hasReplays(replays) && (
                            <CommentList
                                comments={replays}
                                addComment={addComment}
                            />
                        )}
                    </div>
                );
            })}
        </>
    );
};

export default memo(CommentList);
