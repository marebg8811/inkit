import { CommentType } from '../../types';

export type CommentListTypes = {
    comments: CommentType[];
    addComment: (
        comment: CommentType,
        parentId: string | undefined,
        uuid: string
    ) => void;
};
