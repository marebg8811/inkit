import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

import App from './App';

test('Renders start screen', () => {
    const { getByText, getByPlaceholderText } = render(<App />);
    expect(getByText('Say something doge 🐕')).toBeInTheDocument();

    const button = getByText('Add Comment');

    expect(button).toBeInTheDocument();
    fireEvent.click(button);
    getByPlaceholderText('Title');
    getByPlaceholderText('Add you comment here...');
    expect(getByText('Cancel')).toBeInTheDocument();
    expect(getByText('Save Comment')).toBeInTheDocument();
});

test('handles showing comment box when add comment button is clicked', () => {
    const { getByText, getByPlaceholderText } = render(<App />);

    const button = getByText('Add Comment');

    expect(button).toBeInTheDocument();
    fireEvent.click(button);

    expect(getByPlaceholderText('Title')).toBeInTheDocument();
    expect(getByPlaceholderText('Add you comment here...')).toBeInTheDocument();

    expect(getByText('Cancel')).toBeInTheDocument();
    expect(getByText('Save Comment')).toBeInTheDocument();

    expect(button).not.toBeInTheDocument();
});

test('adds a comment', async () => {
    const { getByText, getByPlaceholderText } = render(<App />);

    const button = getByText('Add Comment');

    fireEvent.click(button);

    const title = getByPlaceholderText('Title');
    const body = getByPlaceholderText('Add you comment here...');

    fireEvent.change(title, {
        target: { value: 'fake_title' },
    });
    fireEvent.change(body, {
        target: { value: 'fake_body' },
    });

    await fireEvent.click(getByText('Save Comment'));

    expect(getByText('fake_title')).toBeInTheDocument();
    expect(getByText('fake_body')).toBeInTheDocument();
});

test('clears a comment form', async () => {
    const { getByText, getByPlaceholderText } = render(<App />);

    const button = getByText('Add Comment');

    fireEvent.click(button);

    const title = getByPlaceholderText('Title');
    const body = getByPlaceholderText('Add you comment here...');

    fireEvent.change(title, {
        target: { value: 'fake_title' },
    });
    fireEvent.change(body, {
        target: { value: 'fake_body' },
    });

    await fireEvent.click(getByText('Cancel'));

    expect(title).toHaveValue('');
    expect(body).toHaveValue('');
});
