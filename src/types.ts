export type CommentType = {
    body: string;
    uuid?: string;
    title: string;
    replays?: any;
};
