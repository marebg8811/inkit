import React, { useCallback, useState } from 'react';
import { cloneDeep } from 'lodash';
import './App.css';
import CommentBox from './components/CommentBox';
import { CommentType } from './types';
import CommentList from './components/CommentList';
import { isEqual } from 'lodash';

function App() {
    const [comments, saveComments] = useState([]);

    const searchAndAdd = useCallback(
        (
            arrayToSearch: CommentType[],
            parentId: string | undefined,
            newComment: CommentType
        ): CommentType[] | void => {
            if (!arrayToSearch.length || !parentId) {
                arrayToSearch.push(newComment);
                return arrayToSearch;
            }

            return arrayToSearch.forEach(comment => {
                if (isEqual(comment.uuid, parentId)) {
                    const replays = comment.replays || [];
                    comment.replays = [
                        ...replays,
                        {
                            ...newComment,
                        },
                    ];
                } else if (comment.replays) {
                    searchAndAdd(comment.replays, parentId, newComment);
                }
            });
        },
        []
    );

    const addComment = useCallback(
        (comment: CommentType, parentId: string | undefined, uuid: string) => {
            const newComment = {
                uuid,
                body: comment.body,
                title: comment.title,
            };

            const clonedComments = cloneDeep(comments);

            searchAndAdd(clonedComments, parentId, newComment);

            saveComments([...clonedComments]);
        },
        [comments, saveComments, searchAndAdd]
    );

    return (
        <div className='App'>
            <header className='pt-10'>
                <img
                    className='logo-spin logo mx-auto'
                    src='doge-logo.png'
                    alt='logo'
                />
            </header>
            <div className='body'>
                <div className='w-1/5'>
                    <p>Say something doge &#128021;</p>
                    <CommentBox className='mt-10' addComment={addComment} />
                </div>
                <CommentList comments={comments} addComment={addComment} />
            </div>
        </div>
    );
}

export default App;
