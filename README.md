
## Available Scripts

To Run the project please run from root:

### `yarn start`

To Run the tests please run from root:

### `yarn test`

And it should spin up on localhost:3000


Code is organized 

`src -> components`  

##Libs in use     

`Tailwind` for styling

`lodash` cloneDeep functionality

`uuidv4` generating unique ids